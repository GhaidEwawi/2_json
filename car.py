import json, random

class Car:
    """Car class"""
    def __init__(self, brand = 'brand',
                 production_year = 'Production Year',
                 fuel_type = 'F',
                 color = "Blue",
                 seats = 4):
        self.brand = brand
        self.production_year = production_year
        self.fuel_type = fuel_type
        self.color = color
        self.seats = seats

def pick_random_from_list(list):
    return list[random.randint(0, len(list)-1)]

def generateCar():
    brandList = ['BMW', 'Audi', 'FIAT', 'Ram', 'GMC', 'Chevrolet', 'Alfa Romeo', 'Mercedes', 'Volvo', 'Nissan']
    fuel = ['Bio-Diesel', 'Ethanol', 'Compressed Natural Gas', 'Liquified Petroleum', 'Diesel', 'Gasoline']
    carSeats = [5, 7, 8]
    colors = ['Red', 'Yellow', 'Blue', 'Orange', 'Green', 'Violent']
    tempCar = Car()
    tempCar.brand = pick_random_from_list(brandList)
    tempCar.production_year = random.randint(1990, 2020)
    tempCar.fuel_type = pick_random_from_list(fuel)
    tempCar.color = pick_random_from_list(colors)
    tempCar.seats = pick_random_from_list(carSeats)
    return tempCar

if __name__ == "__main__":

    myList = []
    for i in range(10):
        myList.append(generateCar().__dict__)

    output = json.dumps(myList)
    open('out.json', 'w+').write(output)
