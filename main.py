import os, json

if __name__ == "__main__":

    if os.path.isfile('out.json'):
        data = None
        with open("out.json", "r") as file:
            data = file.read()
        if data is not None:
            myList = json.loads(data)

            for index, item in enumerate(myList):
                print('item number ', index + 1)
                for key, value in item.items():
                    print(key, ':', value)
                print('')

    else:
        print(error)
        exit
